// import mongoose from "mongoose"
const mongoose = require("mongoose")


const todoSchema = new mongoose.Schema({
    userId: String,
    todos: [],
})

module.exports = mongoose.model("ToDo", todoSchema)