const express = require("express")
const cors = require("cors")
// import mongoose from "mongoose"
const mongoose = require("mongoose")
const routes = require("./Router/TodoRoute.js")
const cookieParser = require("cookie-parser")
const TodoModel = require("./Models/TodoModel")


const app = express()
app.use(express.json())
// app.use(express.urlencoded())
app.use(cors())
app.use(cookieParser());

const DB_link = 'mongodb+srv://admin:AxWj03B0vQcNdnPB@cluster0.l0r2dts.mongodb.net/?retryWrites=true&w=majority';

mongoose.connect(DB_link)
.then(function(db){
    console.log("db connected for userModel");
})
.catch(function(err){
    console.log("err for userModel");
    console.log(err);
})

const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String
})

const User = new mongoose.model("User", userSchema)
//Routes
app.post("/register", (req, res)=> {
    const { name, email, password} = req.body
    console.log(req.body)
    User.findOne({email: email}, (err, user) => {
        if(user){
            res.send({message: "User already registerd"})
        } else {
            const user = new User({
                name,
                email,
                password
            })
            user.save(err => {
                if(err) {
                    res.send(err)
                } else {
                    res.send( { message: "Successfully Registered, Please login now." })
                }
            })
        }
    })
}) 

//Routes
app.post("/login",(req, res)=> {
    const { email, password} = req.body
    User.findOne({ email: email}, (err, user) => {
        console.log(user)
        if(user){
            if(password === user.password ) {
                res.cookie('isLoggedIn',true,{httpOnly:true})
                // res.cookie('user',req.body.email,{httpOnly:true})

                // req.cookies.isLoggedIn   
                res.send({message: "Login Successfull", user: user})
            } else {
                res.send({ message: "Password didn't match"})
            }
        } else {
            res.send({message: "User not registered"})
        }
    })
}) 

//Routes add , update , delete 
app.post("/todos",async (req, res)=> {
    const { user, items} = req.body
    console.log("inside post todos")
    console.log("user  " ,user ," items - " , items)
    const userFound = await User.findOne({ email: user.email}).exec()

            if(!userFound || user.password !== userFound.password ) {       
            res.send({message: "User not registered"})
            }

    const todosFound = await TodoModel.findOne({userId:userFound._id}).exec()   
    if(!todosFound){
        console.log("HIIIIIIIIIIIIIIIIIIIIIII")
        TodoModel.create({
            userId:userFound._id,
            todos: items
        })
        res.send({message:"first item added", todosData:items})
        return;
    }
    else{
        todosFound.todos = items
        todosFound.save()
    }

    if(!todosFound){
        res.send({message:"No todos found"})
    }
    else{
        res.send({message:"Added/Updated db", todosData:todosFound.todos})     
    }
}) 

// fetch todos 
app.get("/todos", async (req, res)=> {
    const user = req.query.user
    console.log('user (get request)' , user.email)
    const userFound = await User.findOne({ email: user.email}).exec()
    // console.log('userFound' , userFound.email)
            if(!userFound || user.password !== userFound.password ) {       
            res.send({message: "User not registered"})
            }

    const todosFound = await TodoModel.findOne({userId:userFound._id}).exec()   
    if(!todosFound){
        res.send({message:"No todos found"})
    }
    else{
        // console.log("GET todos first render" , todosFound.todos)
        res.send({message:"list of todos", todosData:todosFound.todos})     
    }
}) 


const port =5000;
app.listen(port,() => {
    console.log("BE started")
})
